﻿using UnityEngine;
using System.Collections;

public class RotationAroundItself : MonoBehaviour {

    public int rotationSpeedPerSecond = 180;

    private ParticleSystem[] particleSystems;
    public ParticleSystem particleThing;

	// Use this for initialization
	void Start () 
    {
        particleSystems = transform.GetComponentsInChildren<ParticleSystem>();
        StartTheSpinning(Color.red);
	}

    public void StartTheSpinning(Color color)
    {
        gameObject.SetActive(true);
        particleThing.startColor = color;
        for (int i = 0; i < particleSystems.Length; i++)
        {
            particleSystems[i].startColor = color;
        }
        StartCoroutine(StartSpinning());
    }
    public void StopTheSpinning()
    {

        StopAllCoroutines();
        gameObject.SetActive(false);
    }
    public void ChangeColorOfGround(Color color)
    {
        particleThing.startColor = color;
    }

    private IEnumerator StartSpinning()
    {
        while(true)
        {
            transform.Rotate(0, rotationSpeedPerSecond * Time.deltaTime, 0);
            yield return null;
        }
    }
}
