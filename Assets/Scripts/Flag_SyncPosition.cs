﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Flag_SyncPosition : NetworkBehaviour {

    [SyncVar]
    private Vector3 syncPos;
    [SerializeField]
    Transform myTransform;
    [SerializeField]
    float lerpRate = 15;

	
    void FixedUpdate()
    {
        LerpPosition();
    }
	
	void LerpPosition()
    {
        //If we are not the local player and we are not the server(Because we update the position on the server)
        if(!isLocalPlayer && !isServer)
        {
            if(Vector3.Distance(myTransform.position,syncPos) > 0.05f)
                myTransform.localPosition = Vector3.Lerp(myTransform.localPosition, syncPos, Time.deltaTime * lerpRate);
        }
    }

    //Gets called by server from domination
    public void UpdateFlagPosition(Vector3 pos)
    {
        //Gets synced because of syncvar
        syncPos = pos;
    }
}
