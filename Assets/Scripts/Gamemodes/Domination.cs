﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class Domination : NetworkBehaviour {

    
    public GameObject MainFlagParent;
    public float rateOfScoring = 0.5f;

    public RotationAroundItself ColorShowing;

    public class PeopleInsideArea
    {
        public Color TeamColor;
        public int AmountOfPeopleInside = 0;
    }

    public List<PeopleInsideArea> PeopleInArea = new List<PeopleInsideArea>();
    public GameObject flag;
    private Flag_SyncPosition syncFlagPos;

    public Color unOwnedMaterialColor;

    public float speedOfRaisingTheFlag = 3;

    
    private Renderer thisGO;

    private int amountOfPeopleInDominationArea = 0;

    private Color lastTeamThatOwnedTheFlag;

    private bool flagAtTop = false;
    private bool isFlagNeutral = true;
    

    private Color lastTeamThatTouchedTheFlag = Color.yellow;
    private Color teamThatMadeTheFlagNeutral = Color.white;

    private bool FirstTimeSome1Enters = false;

    private bool RoundPlaying = false;

    private float speedOfFlagPerFrame;


	// Use this for initialization
	void Start ()
    {
        //Store the renderer of this gameobject
        thisGO = gameObject.GetComponent<Renderer>();


        speedOfFlagPerFrame = 7 / speedOfRaisingTheFlag;
        StartCoroutine(CheckIfTanksAreInsideDominationArea());

        syncFlagPos = flag.GetComponent<Flag_SyncPosition>();
        syncFlagPos.UpdateFlagPosition(flag.transform.localPosition);

        //RpcSyncSpinning(unOwnedMaterialColor.r, unOwnedMaterialColor.g, unOwnedMaterialColor.b, unOwnedMaterialColor.a);
        //RpcStopSpinning();
        ColorShowing.StartTheSpinning(unOwnedMaterialColor);
        ColorShowing.StopTheSpinning();

        ResetDominationMode();
	}


    IEnumerator CheckIfTanksAreInsideDominationArea()
    {
        yield return new WaitForSeconds(0.5f);
        // Collect all the colliders in a sphere from the shell's current position to a radius of the explosion radius.
        //Collider[] colliders = Physics.OverlapSphere(transform.position, m_ExplosionRadius, m_TankMask);
    }

    public void SeeIfAllTanksAreInsideTheArea(GameObject TankThatDied)
    {

        Collider[] colliders = Physics.OverlapSphere(transform.position, transform.lossyScale.x/2, LayerMask.GetMask("Players"));
        if(colliders.Length != 0)
        {
            Debug.Log(colliders.Length + " This is how many tanks there are inside this layer " + colliders[0].transform.GetChild(0).GetChild(0).GetComponent<Renderer>().material.color);
            
        }

        foreach (var item in colliders)
        {
            Debug.Log(item.GetComponent<TankHealth>().m_Manager.m_PlayerColor + "Player colors");
        }
            

        if(colliders.Length != amountOfPeopleInDominationArea)
        {
            foreach (var item in PeopleInArea)
            {
                if (TankThatDied.GetComponent<TankHealth>().m_Manager.m_PlayerColor == item.TeamColor)
                {
                    Debug.Log("Found a tank that has the same color. will delete it now");
                    item.AmountOfPeopleInside--;
                    if (item.AmountOfPeopleInside <= 0)
                    {
                        PeopleInArea.Remove(item);
                        break;
                    }
                }
            }
        }
        else if(colliders.Length == 0)
        {
            PeopleInArea.Clear();
        }
    }
    IEnumerator WaitForTime()
    {
        yield return new WaitForSeconds(0.1f);
        //SeeIfAllTanksAreInsideTheArea(); 
    }
    public void SeeIfAllTanksAreInsideTheArea()
    {

        PeopleInArea.Clear();
        Collider[] colliders = Physics.OverlapSphere(transform.position, transform.lossyScale.x / 2, LayerMask.GetMask("Players"));

        if (colliders.Length == amountOfPeopleInDominationArea)
        {

        }
        else if (colliders.Length == 0)
        {
            PeopleInArea.Clear();
        }
        if (colliders.Length != 0)
        {
            for (int i = 0; i < colliders.Length; i++)
            {
                var AddedATankToCurrentList = false;
                foreach (var item in PeopleInArea)
                {
                    //If a player with the same color as some1 else joins then add 1 to AmountOfPeopleInside
                    if (item.TeamColor == colliders[i].GetComponent<TankHealth>().m_Manager.m_PlayerColor)
                    {
                        item.AmountOfPeopleInside += 1;
                        AddedATankToCurrentList = true;
                    }
                }
                //There were no tanks with the same color.
                //So add a new item to the list with their color and add 1 to AmountOfPeopleInside
                if (!AddedATankToCurrentList)
                    PeopleInArea.Add(new PeopleInsideArea { TeamColor = colliders[i].GetComponent<TankHealth>().m_Manager.m_PlayerColor, AmountOfPeopleInside = 1 });
            }
            AreaOwner();
        }
    }

    [ServerCallback]
    void OnTriggerEnter(Collider other)
    {
        if(!FirstTimeSome1Enters)
        {
            RoundPlaying = true;
            FirstTimeSome1Enters = true;
            StartCoroutine(IsSomeOneScoring());
            
        }
        //If the object is a player then check their color and add them to the list if they are a new player otherwise don't
        if((1 << other.gameObject.layer) == LayerMask.GetMask("Players"))
        {
            amountOfPeopleInDominationArea += 1;

            var AddedATankToCurrentList = false;
            foreach (var item in PeopleInArea)
            {
                //If a player with the same color as some1 else joins then add 1 to AmountOfPeopleInside
                if (item.TeamColor == other.GetComponent<TankHealth>().m_Manager.m_PlayerColor)
                {
                    item.AmountOfPeopleInside += 1;
                    AddedATankToCurrentList = true;
                }
            }
            //There were no tanks with the same color.
            //So add a new item to the list with their color and add 1 to AmountOfPeopleInside
            if(!AddedATankToCurrentList)
                PeopleInArea.Add(new PeopleInsideArea { TeamColor = other.GetComponent<TankHealth>().m_Manager.m_PlayerColor, AmountOfPeopleInside = 1 });

            if (!AddedATankToCurrentList)
                Debug.Log("tank was added to list");
            WHoOwnsTheArea();
        }
    }
    [ServerCallback]
    void OnTriggerExit(Collider other)
    {
        if ((1 << other.gameObject.layer) == LayerMask.GetMask("Players"))
        {
            amountOfPeopleInDominationArea -= 1;
            for (int i = 0; i < PeopleInArea.Count; i++)
            {
                if (PeopleInArea[i].TeamColor == other.GetComponent<TankHealth>().m_Manager.m_PlayerColor)
                {
                    PeopleInArea[i].AmountOfPeopleInside -= 1;
                    if(PeopleInArea[i].AmountOfPeopleInside <= 0)
                    {
                        PeopleInArea.RemoveAt(i);
                    }
                }
            }
            WHoOwnsTheArea();
        }
    }
    [ClientRpc]
    void RpcWhatColorIsFlag()
    {
        StartCoroutine(WaitForTime());
    }

    void WHoOwnsTheArea()
    {
        AreaOwner();
        RpcWhatColorIsFlag();
    }

    void AreaOwner()
    {
        var amountOfTeamsInsideArea = PeopleInArea.Count;
        if (amountOfTeamsInsideArea > 1)
        {
            //More than 1 team is inside the area of influence. Determine who has the most tanks inside.
            //SetFlagSharedMaterialColor(Color.green);

            //TODO If i want to. i can stop the main coroutine and then start a new 1 to check if there is still more than 2 people inside the area. 
            //This means that i stop the team being able to get points(Aka it is being contested so i don't do shit.
        }
        else if (amountOfTeamsInsideArea == 1)
        {
            
            //Debug.Log("The flag got painted");
            //Raise the flag.
            lastTeamThatTouchedTheFlag = PeopleInArea[0].TeamColor;
            if((flagAtTop &&  lastTeamThatTouchedTheFlag == lastTeamThatOwnedTheFlag) )
            {
            }
            else
            {
                Debug.Log("Another touched the flag");
                if(isServer)
                {
                    StopCoroutine("RaiseTheFlag");
                    StartCoroutine("RaiseTheFlag");
                }
                    
                else
                {
                    Debug.Log("You are not the server");
                }
            }
        }
        //Debug.Log(PeopleInArea.Count + " <----- amount of Teams inside the area");
    }
    IEnumerator RaiseTheFlag()
    {
        while (PeopleInArea.Count > 0)
        {
            yield return null;
            if (PeopleInArea.Count < 2)
            {
                if (flag.transform.position.y >= 0 && isFlagNeutral)
                {
                    Debug.Log("flag is at top and flag is not neutral");
                    //The flag reached the top. Stop doing stuff.
                    flagAtTop = true;
                    lastTeamThatOwnedTheFlag = PeopleInArea[0].TeamColor;
                    var temp = flag.transform.position;
                    temp.y = 0;
                    flag.transform.position = temp;

                    //Show that the players now own the flag. 
                    RpcSyncSpinning(lastTeamThatOwnedTheFlag.r, lastTeamThatOwnedTheFlag.g, lastTeamThatOwnedTheFlag.b, lastTeamThatOwnedTheFlag.a);

                    isFlagNeutral = false;
                }
                else if (isFlagNeutral)
                {
                    if (PeopleInArea.Count > 0)
                    {
                        if (teamThatMadeTheFlagNeutral == PeopleInArea[0].TeamColor)
                        {
                            //Raise the flag
                            var temp = flag.transform.position;
                            //Debug.Log("Speed of flag per frame: " + speedOfFlagPerFrame + " delta time: " + Time.deltaTime + " speed per frame * deltatime" + speedOfFlagPerFrame * Time.deltaTime);
                            temp.y += speedOfFlagPerFrame * Time.deltaTime;
                            flag.transform.position = temp;
                        }
                        else if(teamThatMadeTheFlagNeutral != PeopleInArea[0].TeamColor)
                        {
                            isFlagNeutral = false;
                        }
                        else if (lastTeamThatTouchedTheFlag == PeopleInArea[0].TeamColor)
                        {

                            //The flag was neutral start raising it.
                            var temp = flag.transform.position;
                            //Debug.Log("Speed of flag per frame: " + speedOfFlagPerFrame + " delta time: " + Time.deltaTime + " speed per frame * deltatime" + speedOfFlagPerFrame * Time.deltaTime);
                            temp.y += speedOfFlagPerFrame * Time.deltaTime;
                            flag.transform.position = temp;

                        }
                        else
                        {
                            isFlagNeutral = false;
                        }
                    }
                }
                else
                {
                    if (lastTeamThatOwnedTheFlag != lastTeamThatTouchedTheFlag)
                    {
                        flagAtTop = false;
                        //no1 owns the flag anymore. make it stop spinning
                        RpcStopSpinning();

                        if (PeopleInArea.Count > 0)
                        {
                            lastTeamThatTouchedTheFlag = PeopleInArea[0].TeamColor;

                            //Lower The flag till it becomes neutral.
                            var temp = flag.transform.position;
                            //Debug.Log("Speed of flag per frame: " + speedOfFlagPerFrame + " delta time: " + Time.deltaTime + " speed per frame * deltatime" + speedOfFlagPerFrame * Time.deltaTime +"TimesRun: " + i++ + " Time: " + Time.time);
                            temp.y -= speedOfFlagPerFrame * Time.deltaTime;
                            flag.transform.position = temp;

                            if (flag.transform.position.y < -7)
                            {
                                //The flag touched the bottom point.
                                temp.y = -7;
                                flag.transform.position = temp;
                                isFlagNeutral = true;
                                lastTeamThatOwnedTheFlag = Color.yellow;
                                teamThatMadeTheFlagNeutral = PeopleInArea[0].TeamColor;


                                //ColorShowing.ChangeColorOfGround(PeopleInArea[0].TeamColor);
                                RpcChangeColorOfGround(PeopleInArea[0].TeamColor.r, PeopleInArea[0].TeamColor.g, PeopleInArea[0].TeamColor.b, PeopleInArea[0].TeamColor.a);


                                SetFlagSharedMaterialColor(PeopleInArea[0].TeamColor);
                            }
                        }

                    }
                    else if (lastTeamThatOwnedTheFlag == lastTeamThatTouchedTheFlag && !flagAtTop)
                    {
                        //Raise the flag because the flag has gone down a bit but you have not lost ownership yet.
                        //The flag was neutral start raising it.
                        var temp = flag.transform.position;
                        //Debug.Log("Speed of flag per frame: " + speedOfFlagPerFrame + " delta time: " + Time.deltaTime + " speed per frame * deltatime" + speedOfFlagPerFrame * Time.deltaTime);
                        temp.y += speedOfFlagPerFrame * Time.deltaTime;
                        flag.transform.position = temp;
                        if (flag.transform.position.y >= 0)
                        {
                            temp.y = 0;
                            flag.transform.position = temp;
                            isFlagNeutral = true;
                        }
                    }
                }
                //Update the flags position
                syncFlagPos.UpdateFlagPosition(flag.transform.localPosition);
            }
        }
    }

    IEnumerator IsSomeOneScoring()
    {
        float lastTimeSome1Scored = Time.time;
        while (RoundPlaying)
        {
            if(flagAtTop)
            {
                if (lastTimeSome1Scored < Time.time)
                {
                    GameMode.instance.SomeoneScored(lastTeamThatOwnedTheFlag);
                    lastTimeSome1Scored = (Time.time + rateOfScoring);
                }
            }
            yield return null;
        }
    }
    public void ResetScoring()
    {
        RoundPlaying = false;
        FirstTimeSome1Enters = false;
        StopAllCoroutines();
        ResetDominationMode();
    }

    void ResetDominationMode()
    {
        //Set the flag at the lowest position
        var temp = flag.transform.position;
        temp.y = -7;
        flag.transform.position = temp;

        flagAtTop = false;
        isFlagNeutral = true;

        ResetMaterial();

        if(isServer)
        {
            syncFlagPos.UpdateFlagPosition(flag.transform.localPosition);
        }
    }

    public void ResetFlagPosition()
    {
        syncFlagPos.UpdateFlagPosition(flag.transform.localPosition);
    }


    void ResetMaterial()
    {
        SetFlagSharedMaterialColor(unOwnedMaterialColor);
    }
    void SetFlagSharedMaterialColor(Color matColor)
    {
        if(isServer)
            RpcSetFlagMaterialColor(matColor.r, matColor.g, matColor.b, matColor.a);
    }
    void OnApplicationQuit()
    {
        //Sets the color to whatever color is necessary
        thisGO.sharedMaterial.SetColor("_Color", unOwnedMaterialColor);
    }
    //sync that we reset the flag material color
    [ClientRpc]
    public void RpcSetFlagMaterialColor(float r,float g,float b,float a)
    {
        //Sets the color to whatever color is necessary
        thisGO.sharedMaterial.SetColor("_Color", new Color(r,g,b,a));
    }
    //Sync that we are starting to spin and sync the new color
    [ClientRpc]
    public void RpcSyncSpinning(float r,float g,float b,float a)
    {
        //Sync the clients with the current leader's color 
        ColorShowing.StartTheSpinning(new Color(r, g, b, a));
    }
    //Sync that we are stopping the spinning
    [ClientRpc]
    public void RpcStopSpinning()
    {
        //Tells the clients to stop the spinning of the domination node(Makes the player aware that no1 gains score from it)
        ColorShowing.StopTheSpinning();
    }

    //Sync the color of the domination area
    [ClientRpc]
    public void RpcChangeColorOfGround(float r, float g, float b, float a)
    {
        //Sync the color to every client
        ColorShowing.ChangeColorOfGround(new Color(r, g, b, a));
    }

}
