﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class ChooseWhichGameModeToHave : NetworkBehaviour {

    [HideInInspector]
    public Dropdown gamemodeDropdown;

    [SyncVar]
    public int ValueChosen;

    private HoldTheGameMode hgm;
    public GameObject theDD;

    void OnEnable()
    {
        StartCoroutine(meow());
    }

    IEnumerator meow()
    {
        yield return new WaitForSeconds(0.2f);
        theDD = GameObject.Find("_HoldGameMode");
        if (theDD != null)
        {
            gamemodeDropdown = theDD.GetComponent<HoldTheGameMode>().dropdown.GetComponent<Dropdown>();

            Debug.Log("Am i the server: " + isServer);
            gamemodeDropdown.onValueChanged.RemoveAllListeners();
            if (isServer)
            {
                gamemodeDropdown.onValueChanged.AddListener(delegate { GameModeChanged(); });
            }

            if (!isServer)
                gamemodeDropdown.interactable = false;
            if (isServer)
            {
                gamemodeDropdown.interactable = true;
            }

            hgm = GameObject.Find("_HoldGameMode").GetComponent<HoldTheGameMode>();
            hgm.nameOfGameMode = gamemodeDropdown.options[gamemodeDropdown.value].text;

            gamemodeDropdown.value = ValueChosen ;
            yield return new WaitForSeconds(1.0f);
            gamemodeDropdown.value = ValueChosen;
            hgm.nameOfGameMode = gamemodeDropdown.options[ValueChosen].text;
        }
        else
        {
            Debug.Log("dd is null");
        }
    }

	public void GameModeChanged()
    {
        Debug.Log("was called");

        ValueChosen = gamemodeDropdown.value;
        gamemodeDropdown.value = ValueChosen;
        hgm.nameOfGameMode = gamemodeDropdown.options[gamemodeDropdown.value].text;
        
        RpcCurrentGameMode(ValueChosen);
    }
    [ClientRpc]
    public void RpcCurrentGameMode(int gameModeValue)
    {
        
        gamemodeDropdown.value = gameModeValue;
        hgm.nameOfGameMode = gamemodeDropdown.options[gamemodeDropdown.value].text;
    }

    
}
