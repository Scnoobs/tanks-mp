﻿using UnityEngine;
using System.Collections;


public class HoldTheGameMode : MonoBehaviour {

    public GameObject GameModeChooser;

    public GameObject dropdown;


    public string nameOfGameMode = "";

	void Start () 
    {
        //basically a singleton pattern ish. because this lobbymanager is weird
        //We search through all gameobjects and see if there are any other scripts named HoldTheGameMode
        //If its nameOfGameMode is empty then we delete it.
        //If it has text inside then we are sure that is the original gameobject

        if(GameObject.FindObjectsOfType<HoldTheGameMode>().Length > 1)
        {
            if(nameOfGameMode == "")
                Destroy(gameObject);
            
        }

        if(gameObject != null)
        {
            DontDestroyOnLoad(gameObject);
        }
	}

    public void SetGameModeName(string gamemodeName)
    {
        nameOfGameMode = gamemodeName;
    }
}
