﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections.Generic;

public class GameMode : NetworkBehaviour  {

    [System.Serializable]
    public class TeamScore
    {
        public Color teamColor;
        public int score;
    }
    [SerializeField]
    public List<TeamScore> teamScores = new List <TeamScore>();

    static public GameMode instance;

    private Domination[] dominationNodes;
    public int dominationScoreToWin = 100;

    [HideInInspector]
    public string theGameMode;

    private bool PopulatedListOnce = false;

    [HideInInspector]
    public bool StartedPlaying = false;

    public Text[] dominationScoreTexts;
    public Text scoreToWinText;
    public Image[] dominationScoreImages;

	// Use this for initialization
	void Start () {

        //Make this a public static instance
        instance = this;

        //Get the gamemode name
        var name = GameObject.Find("_HoldGameMode").GetComponent<HoldTheGameMode>().nameOfGameMode;
        WhatGameModeIsThis(name);
	}
    //called by start
    public void WhatGameModeIsThis(string gameModeName)
    {
        theGameMode = gameModeName;
        dominationNodes = FindObjectsOfType<Domination>();

        //Disable all the domination texts
        foreach (var item in dominationScoreTexts)
        {
            item.transform.parent.gameObject.SetActive(false);
        }
        foreach (var item in dominationScoreImages)
        {
            item.gameObject.SetActive(false);
        }
        scoreToWinText.transform.parent.gameObject.SetActive(false);
        switch (theGameMode)
        {
            case "Deathmatch":
                Debug.Log("Deathmatch");
                //We aren't playing domination so disable all domination nodes
                foreach (var node in dominationNodes)
                {
                    node.MainFlagParent.SetActive(false);
                }
                break;
            case "Domination":
                Debug.Log("Domination.");
                break;

            default:
                Debug.LogError("No gamemode was found. uhhh dunno what went wrong. Gamemode: " + theGameMode);
                break;
        }
    }

    //active the labels at the top of domination which show the score
    [ClientRpc]
    void RpcShowTheScoreLabels(int ShowWhichItems)
    {
        Debug.Log(ShowWhichItems + " activating labels and shit");
        for (int i = 0; i < ShowWhichItems; i++)
        {
            dominationScoreTexts[i].transform.parent.gameObject.SetActive(true);
        }
        scoreToWinText.transform.parent.gameObject.SetActive(true);
        for (int i = 0; i < ShowWhichItems; i++)
        {
            dominationScoreImages[i].gameObject.SetActive(true);
        }
    }
    
	
	void RefreshList(GameObject tank)
    {
        //For every domination mode area. call see if a current tank is inside that area.
        foreach (var item in dominationNodes)
        {
            item.SeeIfAllTanksAreInsideTheArea(tank);
        }
    }
    public void ATankDied(GameObject tank)
    {
        //If domination is selected then call this
        if (theGameMode == "Domination")
        {
            RefreshList(tank);

            //after x duration. reset the tank
            StartCoroutine(ResetTank(tank));
        }
    }

    //Revives a tank that has died
    IEnumerator ResetTank(GameObject tankThatDied)
    {
        yield return new WaitForSeconds(5);
        RpcResetThisTank(tankThatDied);
        
    }
    [ClientRpc]
    void RpcResetThisTank(GameObject deadTank)
    {
        foreach (var item in GameManager.m_Tanks)
        {
            if (item.m_PlayerNumber == deadTank.GetComponent<TankMovement>().m_PlayerNumber)
            {
                item.Reset();
                item.EnableControl();
            }
        }
    }

    //Someone scored so send the score to every player
    public void SomeoneScored(Color colorThatScored)
    {
        for (int i = 0; i < teamScores.Count; i++)
        {
            if(teamScores[i].teamColor == colorThatScored)
            {
                teamScores[i].score += 1;
                
                //Send the score to the clients
                RpcSendScore(i, teamScores[i].score);
            }
        }
    }

    /// <summary>
    /// Add all the teams color into the scores List
    /// </summary>
    public void PopulateTeamScoreList()
    {

        //TODO check if populatedlistonce needs to be reset every win.
        if (theGameMode == "Domination")
        {


            //Reset Score 
            //RpcResetScore();


            if (!PopulatedListOnce)
            {

                if(isServer)
                {
                    Debug.Log("Just sent the labels.");
                    RpcShowTheScoreLabels(GameManager.m_Tanks.Count);
                    
                }
                
                PopulatedListOnce = true;
                Debug.Log(GameManager.m_Tanks.Count);
                for (int i = 0; i < GameManager.m_Tanks.Count; i++)
                {
                    var aPlayersColor = new TeamScore { teamColor = GameManager.m_Tanks[i].m_PlayerColor, score = 0 };
                    RpcSendStartingList(aPlayersColor.teamColor.r, aPlayersColor.teamColor.g, aPlayersColor.teamColor.b);
                }
            }
        }
    }
    [ClientRpc]
    void RpcSendStartingList(float R,float G,float B)
    {
        teamScores.Add(new TeamScore { teamColor = new Color(R,G,B),score = 0});
        //Change the color of the text to represent the one above.
        dominationScoreTexts[teamScores.Count - 1].transform.parent.GetComponent<Image>().color = teamScores[teamScores.Count - 1].teamColor;
        dominationScoreImages[teamScores.Count - 1].color = teamScores[teamScores.Count - 1].teamColor;
        dominationScoreTexts[teamScores.Count - 1].text = "Points: " + 0;

    }
    [ClientRpc]
    void RpcSendScore(int index,int currentScore)
    {
        teamScores[index].score = currentScore;
        //Update the visual score aswell
        dominationScoreTexts[index].text = "Points: " + currentScore;
    }
    [ClientRpc]
    public void RpcResetScore()
    {
        if(isServer)
        {
            foreach (var item in dominationNodes)
            {
                //Stop all domination coroutines because we have finished the round.
                item.StopAllCoroutines();

                //Reset the particle system for the domiation
                item.RpcChangeColorOfGround(item.unOwnedMaterialColor.r, item.unOwnedMaterialColor.g, item.unOwnedMaterialColor.b, item.unOwnedMaterialColor.a);
                item.RpcStopSpinning();
            }
        }
        //Reset the flags to default state
        foreach (var item in dominationNodes)
        {
            item.ResetScoring();
        }
        for (int i = 0; i < teamScores.Count; i++)
        {
            teamScores[i].score = 0;
            if(isServer)
                RpcSendScore(i, 0);
            Debug.Log("Resetting score");
        }
    }

    public bool PlayerReachedMaxScore()
    {
        int maxScore = 0;
        for (int i = 0; i < teamScores.Count; i++)
        {
            if(teamScores[i].score > maxScore)
            {
                maxScore = teamScores[i].score;
            }
        }
        return maxScore >= dominationScoreToWin;
    }
}
