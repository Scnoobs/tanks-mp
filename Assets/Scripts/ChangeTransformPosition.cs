﻿using UnityEngine;
using System.Collections;

public static class ChangeTransformPosition {

    public static Vector3 ChangeXPosition(Vector3 CurrentPosition,  float newXNumber)
    {
        CurrentPosition.Set(newXNumber, CurrentPosition.y, CurrentPosition.z);
        return CurrentPosition;
    }
    public static Vector3 ChangeYPosition(Vector3 CurrentPosition, float newYNumber)
    {
        CurrentPosition.Set(CurrentPosition.x, newYNumber, CurrentPosition.z);
        return CurrentPosition;
    }
    public static Vector3 ChangeZPosition(Vector3 CurrentPosition, float newZNumber)
    {
        CurrentPosition.Set(CurrentPosition.x,CurrentPosition.z, newZNumber);
        return CurrentPosition;
    }
}
